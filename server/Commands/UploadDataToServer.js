const SendToServer = require('./SendToServer');
const ConvertVideo = require('./ConvertVideo');

const processRequests = (processedRequests, rows, db) => {
    if (processedRequests === rows.length) {
        setTimeout(() => UploadDataToServer(db), 1000);
    }
};

const processServer = async (processedRequests, rows, db, data) => {
    // Update this entry, to let the next loop know
    // this entry is being sent to the server.
    await db.run("UPDATE `users` SET `sending_to_server` = 1 WHERE `guid` = $guid", {$guid: data.guid});

    // POST data to server.
    SendToServer(db, data, async ({success}) => {
        let fileHash = '';

        if (data.type === 'photo') {
            fileHash = data.photo.split('uploads/').join('').split('.png').join('');
        } else {
            fileHash = data.video.split('uploads/').join('').split('.webm').join('').split('.mp4').join('');
        }

        // Update this entry, to let the loop know
        // this entry has been sent to the server.
        if (data.type === 'photo') {
            await db.run("UPDATE `users` SET `sending_to_server` = 0, `sent_to_server` = $sent_to_server, `photo` = $photo WHERE `guid` = $guid", {
                $sent_to_server: success ? 1 : 0,
                $photo: `uploads/${fileHash}.png`,
                $guid: data.guid
            });
        } else {
            await db.run("UPDATE `users` SET `sending_to_server` = 0, `sent_to_server` = $sent_to_server, `video` = $video WHERE `guid` = $guid", {
                $sent_to_server: success ? 1 : 0,
                $video: `uploads/${fileHash}.mp4`,
                $guid: data.guid
            });
        }

        processedRequests++;
        processRequests(processedRequests, rows, db);
    });
};

const UploadDataToServer = (async db => {
    let processedRequests = 0;

    // Get all entries that have not yet been sent to the
    // server and are not currently sending to the server.
    const rows = await db.all("SELECT * FROM `users` WHERE `sent_to_server` = 0 AND `sending_to_server` = 0");

    if (!rows || rows.length === 0) {
        setTimeout(() => UploadDataToServer(db), 1000);
        return;
    }

    rows.map(async data => {
        if (data.type === 'photo') {
            await processServer(processedRequests, rows, db, data);
            return;
        }

        ConvertVideo(data, async err => {
            if (err) {
                processedRequests++;
                processRequests(processedRequests, rows, db);
                console.error(err);
                return;
            }

            await processServer(processedRequests, rows, db, data);
        });
    });
});

module.exports = UploadDataToServer;
