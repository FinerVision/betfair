import React from "react";

export default props => (
    <div className="Terms">
        <p>
            <strong>Ascot Commentary Challenge Competition</strong>
        </p>
        <p>
            <strong>Terms and Conditions</strong>
        </p>
        <ol>
            <li>
                In order to take part in this competition, simply take part in the
                commentary challenge (further details of which shall be made known to
                you before you take part) and, after we have sent a video to you of
                your participation in the challenge, upload it to your Twitter,
                Facebook and/or Instagram account along with the tag line
                #commentarychallenge.
            </li>
            <br/>
            <li>
                This competition is open to anyone aged at least 25 years old who is a
                UK resident, excluding employees of Betfair or any of its affiliated
                companies, their families, agents or anyone else connected with this
                competition. Betfair reserves the right to verify the eligibility of
                entrants.
            </li>
            <br/>
            <li>
                The entry period starts at 10:00AM (GMT) on Saturday 17<sup>th</sup>
                February 2018 and ends at 18:00PM (GMT) on Saturday 17<sup>th</sup>
                February 2018. Entries received after the stipulated closing time will
                be invalid and will not be eligible to win a prize.
            </li>
            <br/>
            <li>
                Entries not complying with these terms and conditions will be invalid.
            </li>
            <br/>
            <li>
                The prize shall be either of the following (to be agreed between
                Betfair and the competition’s winner):
            </li>
            <br/>
            <li>
                two (2) places on a VIP trip to Paul Nicholls’ yard (at a date to
                be confirmed); or
            </li>
            <br/>
            <li>
                two (2) places in the Betfair box at the Emirates stadium for the
                2017/18 season (at a match decided by Betfair).
            </li>
            <br/>
            <li>
                No payment is required to enter the competition.
            </li>
            <br/>
            <li>
                The winner will be chosen by Betfair (in its sole discretion) from all
                eligible entries which were received before the competition closing
                time. The winner will be notified, by email, following the competition
                closing time. In the event that the winner fails to respond to Betfair
                to confirm its acceptance of the prize within 2 weeks of initial
                contact, Betfair reserves the right (in its sole discretion) to forfeit
                the prize or to select an alternative winner.
            </li>
            <br/>
            <li>
                Betfair reserves the right to request valid proof of identification (as
                determined in its sole discretion) from the winner prior to awarding
                the prize.
            </li>
            <br/>
            <li>
                Betfair will not be responsible for any inability of a winner to take
                up the specified prize.
            </li>
            <br/>
            <li>
                Betfair reserves the right to provide a substitute prize (or prizes) of
                similar value in the event that the specified prize is unavailable for
                reasons beyond its reasonable control. Cash or credit alternatives will
                not be offered in any circumstances and the prize is not transferable
                for any alternative benefit.
                <br/>
                The winner is responsible for expenses and arrangements which are not
                referred to in these terms and conditions (including any necessary
                travel or accommodation which is necessary in order to receive either
                of the prizes referred to above). Prizes are subject to availability
                and the prize suppliers' terms and conditions.
            </li>
            <br/>
            <li>
                By entering the competition, entrants agree to their name, image/video
                and country of residence being made publicly available if they win.
            </li>
            <br/>
            <li>
                All entries and any copyright subsisting in the entries become and
                remain the property of Betfair. Betfair accepts no responsibility for
                incorrectly completed, lost or delayed entries.
            </li>
            <br/>
            <li>
                Betfair reserves the right to suspend, cancel or amend the competition
                and/or review and revise these terms and conditions at any time without
                giving prior notice. By continuing to take part in the competition
                after any revision of these terms and conditions, entrants shall be
                deemed to have agreed to any such new or amended terms.
            </li>
            <br/>
            <li>
                This competition is governed by the laws of England and Wales and is
                subject to the exclusive jurisdiction of the courts of England and
                Wales.
            </li>
        </ol>
    </div>
);
