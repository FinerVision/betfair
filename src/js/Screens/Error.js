import React, {Component} from "react";
import Store from "fv-store";
import config from "../../../config";

export default class Error extends Component {
    constructor(props) {
        super(props);

        this.mounted = false;

        this.state = {
            devices: []
        };
    }

    componentDidMount() {
        this.mounted = true;
        this.setDevices();
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    setDevices() {
        navigator.mediaDevices.enumerateDevices().then(devices => {
            devices = devices
                .filter(device => /audio|video/.test(device.kind) && !/Default|Built-in/.test(device.label))
                .map(device => {
                    return {
                        label: device.label,
                        kind: device.kind
                    };
                });

            if (this.mounted) {
                this.setState({devices}, () => this.setDevices());
            }
        });
    }

    onlyUsePhotoApp() {
        Store.set({onlyUsePhotoApp: true});
        this.props.history.replace('/');
    }

    render() {
        return (
            <div className="Error">
                <div className="Error__message">
                    <p>
                        There is a hardware error!
                    </p>

                    <p>
                        Make sure the camera and both headsets are correctly connected.
                    </p>

                    <p>
                        Once connected, tap the refresh button.
                    </p>

                    <button onClick={() => location.href = config.get('url')}>Refresh</button>
                    <button onClick={() => this.onlyUsePhotoApp()}>Only use photo app</button>

                    <p style={{marginTop: '50px'}}>
                        Connected devices
                    </p>

                    <ul>
                        {this.state.devices.map((device, index) => <li key={index}>{device.kind} – {device.label}</li>)}
                    </ul>
                </div>
            </div>
        );
    }
}
