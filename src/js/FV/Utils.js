import config from "../../../config";

const scale = (number, axis) => {
    let scale = window.innerWidth / config.get('resolution.width');
    let scaledNumber = (100 / window.innerWidth) * (number * scale);

    if (!axis) {
        return number * scale;
    }

    if (axis === 'y') {
        scale = window.innerHeight / config.get('resolution.height');
        scaledNumber = (100 / window.innerHeight) * (number * scale);
    }

    return scaledNumber;
};

const preloadImages = (images, callback) => {
    const loadedImages = [];

    for (let i = 0; i < images.length; i++) {
        const img = new Image;
        img.src = images[i];
        img.onload = () => {
            loadedImages.push(i);

            if (callback && loadedImages.length === images.length) {
                callback();
            }
        };
    }
};

const getLocation = (id = null) => {
    if (id === null) {
        id = localStorage.getItem('location');
    }

    const locations = config.get('locations');
    for (let i = 0; i < locations.length; i++) {
        if (locations[i].id === id) {
            return locations[i];
        }
    }
    return null;
};

export default {scale, preloadImages, getLocation};
