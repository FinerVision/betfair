import config from "../../../config";

export default {
    getDevices() {
        const cameraLabelRegex = /HD Pro Webcam C920/i;
        const micLabelRegex = /Lync|Apowersoft/i;
        const CONSTRAINTS = {
            audio: true,
            video: {width: {exact: 1920}, height: {exact: 1080}}
        };

        return new Promise((resolve, reject) => {
            navigator.mediaDevices.getUserMedia(CONSTRAINTS).then(() => {
                navigator.mediaDevices.enumerateDevices().then(devices => {
                    const speakers = devices.filter(device => device.kind === 'audiooutput' && micLabelRegex.test(device.label));
                    const speakerIds = speakers.map(speaker => speaker.deviceId);
                    const cameraId = devices.filter(device => device.kind === 'videoinput' && cameraLabelRegex.test(device.label))[0].deviceId;
                    const mics = devices.filter(device => device.kind === 'audioinput' && micLabelRegex.test(device.label));
                    const micIds = mics.map(mic => mic.deviceId);

                    const permissionRequests = [
                        navigator.mediaDevices.getUserMedia({video: {deviceId: {exact: cameraId}}, audio: false})
                    ];

                    micIds.map(micId => {
                        const request = navigator.mediaDevices.getUserMedia({
                            video: false,
                            audio: {deviceId: {exact: micId}}
                        });
                        permissionRequests.push(request);
                    });

                    Promise.all(permissionRequests).then(streams => {
                        if (streams.length < 3 && config.get('env') === 'production') {
                            reject("Not enough streams. Probably there isn't the correct number of devices connected. Streams: ", streams);
                            return;
                        }

                        resolve({
                            speakerIds,
                            micIds,
                            devices,
                            streams: {
                                video: streams[0],
                                audio: [streams[1], streams[2]]
                            }
                        });
                    }).catch(reject);
                }).catch(reject);
            }).catch(reject);
        });
    }
};
