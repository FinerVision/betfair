import React, {Component} from "react";
import Store from "fv-store";
import Screen from "../Components/Screen";
import Touchable from "../Components/Touchable";

export default class Start extends Component {
    constructor(props) {
        super(props);
    }

    selectType(type) {
        if (type === 'video') {
            Store.set({onlyUsePhotoApp: false});
        }
        this.props.history.replace(`/${type}/landing`);
    }

    render() {
        return (
            <Screen name="Start">
                <Touchable
                    x={182}
                    y={756}
                    width={591}
                    height={151}
                    onPress={() => this.selectType('photo')}
                />
                <Touchable
                    x={1140}
                    y={756}
                    width={591}
                    height={151}
                    onPress={() => this.selectType('video')}
                />
            </Screen>
        );
    }
}
