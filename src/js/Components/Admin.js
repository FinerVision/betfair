import React, {Component} from "react";
import Touchable from "./Touchable";

class Admin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pressed: false
        };
    }

    togglePressed() {
        this.setState({pressed: !this.state.pressed});
    }

    action(type) {
        this.togglePressed();

        if (type === 'reset') {
            this.props.history.replace('/start');
        }

        if (type === 'setup') {
            this.props.history.replace('/setup');
        }

        if (type === 'info') {
            this.props.history.replace('/info');
        }
    }

    render() {
        if (!this.state.pressed) {
            return (
                <Touchable
                    className="Admin__touchable"
                    x={0}
                    y={0}
                    width={80}
                    height={80}
                    onPress={() => this.togglePressed()}
                />
            );
        }

        return (
            <div className="Admin">
                <div className="Admin__confirm">
                    <div className="Admin__confirm-title">What do you want to do?</div>

                    <div className="Admin__confirm-buttons">
                        <button onClick={() => this.action('reset')} className="Admin__confirm-buttons--reset">Reset</button>
                        <button onClick={() => this.action('info')} className="Admin__confirm-buttons--info">Info</button>
                        <button onClick={() => this.action('setup')} className="Admin__confirm-buttons--setup">Setup</button>
                    </div>
                </div>

                <div className="Admin__background" onClick={() => this.togglePressed()}/>
            </div>
        );
    }
}

export default Admin;
