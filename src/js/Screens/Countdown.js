import React, {Component} from "react";
import Screen from "../Components/Screen";
import Utils from "../FV/Utils";
import Timer from "../FV/Timer";

export default class Countdown extends Component {
    constructor(props) {
        super(props);

        this.location = Utils.getLocation();
        this.countdowns = {
            3: {
                instruction: 'Put your headphones on',
                timeout: 2000,
                icon: 'headphones'
            },
            2: {
                instruction: 'Know your colours',
                timeout: 3500
            },
            1: {
                instruction: 'And they’re off!',
                timeout: 1000,
                icon: 'horse'
            }
        };

        this.state = {
            countdown: 3
        };
    }

    componentDidMount() {
        this.countdown();
    }

    componentWillUnmount() {
        Timer.clearTimeouts();
    }

    countdown() {
        Timer.setTimeout(() => {
            const countdown = this.state.countdown - 1;
            if (countdown === 0) {
                this.props.history.replace(`/${this.props.match.params.type}/record`);
                return;
            }
            this.setState({countdown}, () => this.countdown());
        }, this.countdowns[this.state.countdown].timeout);
    }

    renderIcon() {
        const {icon} = this.countdowns[this.state.countdown];

        if (!icon) {
            return null;
        }

        return <img className="Countdown__icon" src={`img/icons/${icon}.png`}/>;
    }

    renderJerseys() {
        if (this.state.countdown !== 2) {
            return null;
        }

        return (
            <div className="Countdown__jerseys">
                {this.location.jerseys.map((jersey, index) => (
                    <div key={index} className="Countdown__jerseys-jersey">
                        <img src={`img/jerseys/${this.location.id}/${jersey}.png`}/>
                    </div>
                ))}
            </div>
        );
    }

    render() {
        return (
            <Screen name="Countdown">
                <div className="Countdown__countdown">
                    {this.state.countdown}
                </div>

                <div className="Countdown__instruction">
                    {this.countdowns[this.state.countdown].instruction}
                </div>

                {this.renderIcon()}
                {this.renderJerseys()}
            </Screen>
        );
    }
}
