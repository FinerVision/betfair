import React, {Component} from "react";
import PropTypes from "prop-types";

class Screen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={`Screen ${this.props.name}`} style={this.props.style}>
                {this.props.children}
            </div>
        );
    }
}

Screen.propTypes = {
    name: PropTypes.string.isRequired,
    style: PropTypes.object
};

Screen.defaultProps = {
    style: {}
};

export default Screen;
