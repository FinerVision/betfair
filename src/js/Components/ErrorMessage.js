import React, {Component} from "react";
import PropTypes from "prop-types";

class ErrorMessage extends Component {
    constructor(props) {
        super(props);
    }

    renderErrorMessages() {
        if (this.props.errors.length > 0) {
            return this.props.errors.map((error, index) => (
                <div key={index} className="ErrorMessage__error">
                    {error}
                </div>
            ));
        }

        return (
            <div className="ErrorMessage__error">
                {this.props.error}
            </div>
        );
    }

    render() {
        if (this.props.errors.length === 0 && !this.props.error) {
            return null;
        }

        return (
            <div className="ErrorMessage">
                {this.renderErrorMessages()}
            </div>
        );
    }
}

ErrorMessage.propTypes = {
    errors: PropTypes.array,
    error: PropTypes.string
};

ErrorMessage.defaultProps = {
    errors: []
};

export default ErrorMessage;
