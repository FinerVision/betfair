const env = require('../env');

module.exports = (function () {
    const {APP_ENV, HANDLE_ERRORS, APP_PORT} = env;

    const settings = {
        port: APP_PORT,
        url: `http://localhost:${APP_PORT}`,
        env: APP_ENV,
        handleErrors: HANDLE_ERRORS,
        volume: 0.3,
        serverUrl: {
            dev: 'http://192.168.15.201/BetFairServer/public/upload.php',
            prod: 'https://commentary-challenge.com/upload.php'
        },
        resolution: {
            width: 1920,
            height: 1080
        },
        collage: {
            canvas: {
                width: 837,
                height: 537
            },
            image: {
                width: 395,
                height: 222
            }
        },
        locations: [
            {
                id: 'haydock',
                timer: 75,
                jerseys: [1, 2, 3, 4, 5, 6, 7, 8],
                x: 379,
                y: 269,
                width: 543,
                height: 310,
                available: true,
                jumps: 4
            },
            {
                id: 'sandown',
                timer: 75,
                jerseys: [1, 2, 3, 4, 5, 6],
                x: 969,
                y: 269,
                width: 543,
                height: 310,
                available: true,
                jumps: 3
            },
            {
                id: 'newbury',
                timer: 64,
                jerseys: [1, 2, 3, 4, 6, 10, 11, 13],
                x: 379,
                y: 615,
                width: 543,
                height: 310,
                available: true,
                jumps: 3
            },
            {
                id: 'ascot',
                timer: 65,
                jerseys: [1, 2, 3, 4, 5, 6],
                x: 969,
                y: 615,
                width: 543,
                height: 310,
                available: true,
                jumps: 3
            }
        ],
        keyboardLayout: [
            ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '-'],
            ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '@'],
            ['z', 'x', 'c', 'v', 'b', 'n', 'm', '.', '_']
        ]
    };

    return {
        get(key, defaultValue = null) {
            const keys = key.split('.');
            let value = settings;

            for (let i = 0; i < keys.length; i++) {
                const key = keys[i];

                if (!value.hasOwnProperty(key)) {
                    console.log(defaultValue);
                    return defaultValue;
                }

                if (Object.keys(value[key]).includes('dev', 'prod')) {
                    value = value[key][APP_ENV];
                } else {
                    value = value[key];
                }
            }

            return value;
        }
    };
})();
