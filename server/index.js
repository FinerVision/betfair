const fs = require('fs');
const express = require('express');
const db = require('sqlite');
const Promise = require('bluebird');
const config = require('../config');
const Utils = require('./FV/Utils');
const middleware = require('./middleware');
const routes = require('./routes');
const bootstrap = require('./bootstrap');

const app = express();

const migrationConfig = {
    force: false,
    migrationsPath: `${__dirname}/../db/migrations`
};

if (!fs.existsSync(`${__dirname}/../db/database.sqlite`)) {
    migrationConfig.force = 'last';
}

if (!fs.existsSync(`${__dirname}/../server/device.txt`)) {
    fs.writeFileSync(`${__dirname}/../server/device.txt`, Utils.guid());
}

Promise
    .resolve()
    .then(() => db.open(`${__dirname}/../db/database.sqlite`, {Promise}))
    .then(() => db.migrate(migrationConfig))
    .catch(err => console.error(err.stack))
    .finally(() => {
        middleware(express, app);
        routes(app, db);
        bootstrap(db);
        app.listen(config.get('port'), () => console.log(`Listening on port ${config.get('port')}`));
    });
