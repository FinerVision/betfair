import React, {Component} from "react";
import Timer from "../FV/Timer";

export default class Info extends Component {
    constructor(props) {
        super(props);

        this.mounted = false;

        this.state = {
            devices: []
        };
    }

    componentDidMount() {
        this.mounted = true;
        this.setDevices();
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    setDevices() {
        navigator.mediaDevices.enumerateDevices().then(devices => {
            devices = devices
                .filter(device => /audio|video/.test(device.kind) && !/Default|Built-in/.test(device.label))
                .map(device => {
                    return {
                        label: device.label,
                        kind: device.kind
                    };
                });

            if (this.mounted) {
                this.setState({devices}, () => this.setDevices());
            }
        });
    }

    render() {
        return (
            <div className="Info">
                <div className="Info__message">
                    <p>
                        Devices
                    </p>

                    <ul>
                        {this.state.devices.map((device, index) => <li key={index}>{device.kind} – {device.label}</li>)}
                    </ul>
                </div>
            </div>
        );
    }
}
