const fs = require('fs');
const {exec} = require('child_process');

const deleteFile = file => {
    if (fs.existsSync(file)) {
        fs.unlinkSync(file);
    }
};

module.exports = (data, callback) => {
    const fileHash = data.video.split('uploads/').join('').split('.webm').join('').split('.mp4').join('');
    const files = {
        uploaded: `${__dirname}/../../public/uploads/${fileHash}.webm`,
        converted: `${__dirname}/../../public/uploads/${fileHash}-converted.mp4`,
        location: `${__dirname}/../../public/video/small/${data.location}.mp4`,
        overlay: `${__dirname}/../../public/img/overlay.png`,
        final: `${__dirname}/../../public/uploads/${fileHash}.mp4`
    };

    // Final file already exists if there is no uploaded file
    // and converted files, and the final file exists.
    if (!fs.existsSync(files.uploaded) && !fs.existsSync(files.converted) && fs.existsSync(files.final)) {
        callback();
        return;
    }

    // Convert uploaded file from webm to mp4
    exec(`ffmpeg -y -i '${files.uploaded}' -r 15 -cpu-used 5 -c:v libx264 -crf 30 -c:a aac -strict -2 -movflags faststart -preset ultrafast -vf scale=1280:720 '${files.converted}'`, err => {
        if (err) {
            console.error('Error in step 1: ', err.message);

            // Uploaded file was not converted. Delete if is somehow exists.
            deleteFile(files.converted);
            callback(err);
            return;
        }

        // Generate final file from converted file.
        let command = `ffmpeg -y -i '${files.converted}' -i '${files.location}' -i '${files.overlay}' -strict -2 -movflags faststart -preset ultrafast -filter_complex '[0][1]overlay=886:469[location];[location][2]overlay=0:0[final]' -map '[final]' -map 0:a -c:a copy '${files.final}'`;
        if (data.age === 'under-25') {
            command = `ffmpeg -y -i '${files.converted}' -i '${files.location}' -strict -2 -movflags faststart -preset ultrafast -filter_complex '[0][1]overlay=886:469[final]' -map '[final]' -map 0:a -c:a copy '${files.final}'`;
        }

        exec(command, err => {
            if (err) {
                console.error('Error in step 2: ', err.message);

                // Final file was not generated. Delete if is somehow exists.
                deleteFile(files.final);
                callback(err);
                return;
            }

            // Delete uploaded and converted files.
            deleteFile(files.uploaded);
            deleteFile(files.converted);
            callback();
        });
    });
};
