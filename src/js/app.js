import React from "react";
import {render} from "react-dom";
import {HashRouter} from "react-router-dom";
import App from "./Components/App";

const routes = (
    <HashRouter>
        <App/>
    </HashRouter>
);

render(routes, document.querySelector('#app'));
