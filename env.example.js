module.exports = {
    APP_ENV: 'prod',
    APP_PORT: 3000,
    HANDLE_ERRORS: true
};
