(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

function mix() {
    var streams = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    var audioContext = new AudioContext();
    var dest = audioContext.createMediaStreamDestination();
    streams.map(function (stream) {
        var source = audioContext.createMediaStreamSource(stream);
        source.connect(dest);
    });
    return dest.stream.getTracks()[0];
}

function getDevices() {
    var cameraLabelRegex = /HD Pro Webcam C920/i;
    var micLabelRegex = /Lync USB Headset/i;
    var CONSTRAINTS = {
        audio: true,
        video: { width: { exact: 1920 }, height: { exact: 1080 } }
    };

    return new Promise(function (resolve, reject) {
        navigator.mediaDevices.getUserMedia(CONSTRAINTS).then(function () {
            navigator.mediaDevices.enumerateDevices().then(function (devices) {
                var speakers = devices.filter(function (device) {
                    return device.kind === 'audiooutput' && micLabelRegex.test(device.label);
                });
                var speakerIds = speakers.map(function (speaker) {
                    return speaker.deviceId;
                });
                var cameraId = devices.filter(function (device) {
                    return device.kind === 'videoinput' && cameraLabelRegex.test(device.label);
                })[0].deviceId;
                var mics = devices.filter(function (device) {
                    return device.kind === 'audioinput' && micLabelRegex.test(device.label);
                });
                var micIds = mics.map(function (mic) {
                    return mic.deviceId;
                });

                var permissionRequests = [navigator.mediaDevices.getUserMedia({ video: { deviceId: { exact: cameraId } }, audio: false })];

                micIds.map(function (micId) {
                    var request = navigator.mediaDevices.getUserMedia({
                        video: false,
                        audio: { deviceId: { exact: micId } }
                    });
                    permissionRequests.push(request);
                });

                Promise.all(permissionRequests).then(function (streams) {
                    resolve({
                        speakerIds: speakerIds,
                        micIds: micIds,
                        streams: {
                            video: streams[0],
                            audio: [streams[1], streams[2]]
                        }
                    });
                });
            }).catch(reject);
        }).catch(reject);
    });
}

getDevices().then(function (ctx) {
    ctx.streams.audio.map(function (stream, index) {
        var audio = document.createElement('audio');
        audio.autoplay = true;
        audio.src = URL.createObjectURL(stream);
        audio.setSinkId(ctx.speakerIds[ctx.speakerIds.length - (index + 1)]);
        document.querySelector('#app').appendChild(audio);
    });
}).catch(function (err) {
    return console.error(err);
});

},{}]},{},[1]);

//# sourceMappingURL=test.js.map
