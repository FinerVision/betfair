import React, {Component} from "react";
import request from "superagent";
import config from "../../../config/index";
import Utils from "../FV/Utils";
import Terms from "../Components/Terms";
import Screen from "../Components/Screen";
import Touchable from "../Components/Touchable";
import Checkbox from "../Components/Checkbox";
import ErrorMessage from "../Components/ErrorMessage";
import VideoResult from "../Components/VideoResult";
import Keyboard, {KeyboardButton, LatinLayout} from 'react-screen-keyboard';
import Timer from "../FV/Timer";

export default class Data extends Component {
    constructor(props) {
        super(props);

        this.location = Utils.getLocation();
        this.type = this.props.match.params.type;
        this.photo = localStorage.getItem('photo');
        this.video = localStorage.getItem('video');
        this.keyboardLayout = LatinLayout;
        this.keyboardLayout.layout = config.get('keyboardLayout');

        this.validation = {
            firstName: 'required',
            surname: 'required',
            email: 'required|email',
            terms: 'required',
            age: 'required'
        };

        this.state = {
            firstName: '',
            surname: '',
            email: '',
            age: '',
            terms: true,
            focusingInput: null,
            errors: [],
            submitting: false,
            isTermsShowing: false,
            comms: true
        };
    }

    componentWillUnmount() {
        Timer.clearTimeouts();
    }

    validate() {
        const errorMessages = {
            required: 'The :field field is required',
            email: 'The :field field is not a valid email',
            checked: 'The :field field needs to be checked'
        };

        const tests = {
            required(str = '') {
                return str.trim().length > 0;
            },
            email(str) {
                return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(str);
            }
        };

        const errors = [];

        for (let field in this.validation) {
            if (!this.validation.hasOwnProperty(field)) {
                continue;
            }

            this.validation[field].split('|').map(rule => {
                const passed = tests[rule](this.refs[field].value);

                if (!passed) {
                    errors.push(errorMessages[rule].split(':field').join(field));
                }
            });
        }
        return {
            errors,
            passed: errors.length === 0
        }
    }

    submit() {
        if (this.state.submitting) {
            return;
        }

        this.setState({submitting: true}, () => {
            const {passed, errors} = this.validate();

            if (!passed) {
                this.setState({errors, submitting: false});
                return;
            }

            this.setState({errors: []});

            const formData = new FormData;
            formData.append('first_name', this.refs.firstName.value);
            formData.append('surname', this.refs.surname.value);
            formData.append('email_address', this.refs.email.value);

            if (this.type === 'photo') {
                formData.append('photo', this.photo);
            } else {
                formData.append('video', this.video);
            }

            formData.append('type', this.type);
            formData.append('location', config.get('env') === 'dev' ? 'test' : this.location.id);
            formData.append('terms', Boolean(this.state.terms));
            formData.append('age', this.state.age);
            formData.append('comms', Boolean(this.state.comms));

            request
                .post(`${config.get('url')}/submit`)
                .send(formData)
                .end(err => {
                    this.setState({submitting: false});

                    if (err) {
                        this.setState({errors: [err.message]});
                        return;
                    }

                    this.props.history.replace(`/${this.props.match.params.type}/thanks`);
                });
        });
    }

    toggleCheckbox(name, checked) {
        const state = this.state;
        state[name] = checked ? true : '';
        this.setState(state);
    }

    renderKeyboard() {
        const {focusingInput} = this.state;

        if (!focusingInput) {
            return null;
        }

        return (
            <div className="Keyboard">
                <Keyboard
                    inputNode={focusingInput}
                    onChange={letter => {
                        focusingInput.onChange({target: {value: letter}})
                    }}
                    leftButtons={[
                        <KeyboardButton
                            key={1}
                            onClick={() => this.setState({focusingInput: null})}
                            value="Close"
                            classes="keyboard-close-button"
                        />
                    ]}
                    rightButtons={[
                        <KeyboardButton
                            key={2}
                            onClick={() => {
                                const {selectionStart, value} = focusingInput;
                                const valueStart = value.substring(0, selectionStart);
                                const valueEnd = value.substring(selectionStart);
                                focusingInput.value = `${valueStart}.com${valueEnd}`;
                                setTimeout(() => {
                                    focusingInput.setSelectionRange(selectionStart + 4, selectionStart + 4);
                                }, 100);
                            }}
                            value=".com"
                        />
                    ]}
                    layouts={[this.keyboardLayout]}
                />
            </div>
        );
    }

    handleFocus(focusingInput) {
        Timer.clearTimeouts();
        this.setState({focusingInput});
    }

    renderResult() {
        if (this.type === 'photo') {
            return (
                <div className="Data__result">
                    <img src={this.photo}/>
                </div>
            );
        }
        return <VideoResult video={this.video} location={this.location}/>;
    }

    render() {
        return (
            <Screen name="Data" style={{backgroundImage: `url(img/screens/${this.type}/data.png)`}}>
                <Touchable
                    x={1137}
                    y={849}
                    width={733}
                    height={194}
                    onPress={() => this.submit()}
                />

                <ErrorMessage errors={this.state.errors}/>

                <div className="Data__form">
                    <div className="Data__form-field Data__form-field--input">
                        <input type="text" ref="firstName" onFocus={() => this.handleFocus(this.refs.firstName)}/>
                    </div>

                    <div className="Data__form-field Data__form-field--input">
                        <input type="text" ref="surname" onFocus={() => this.handleFocus(this.refs.surname)}/>
                    </div>

                    <div className="Data__form-field Data__form-field--input">
                        <input type="text" ref="email" onFocus={() => this.handleFocus(this.refs.email)}/>
                    </div>

                    <Checkbox
                        checked={Boolean(this.state.terms)}
                        className="Data__form-field Data__form-field--checkbox Data__form-field--checkbox-terms"
                        onChange={checked => this.toggleCheckbox('terms', checked)}
                    />

                    <Checkbox
                        checked={this.state.age === 'under-25'}
                        className="Data__form-field Data__form-field--checkbox Data__form-field--checkbox-under25"
                        onChange={checked => this.setState({age: 'under-25'})}
                    />

                    <Checkbox
                        checked={this.state.age === 'over-25'}
                        className="Data__form-field Data__form-field--checkbox Data__form-field--checkbox-over25"
                        onChange={checked => this.setState({age: 'over-25'})}
                    />

                    <Checkbox
                        checked={Boolean(this.state.comms)}
                        className="Data__form-field Data__form-field--checkbox Data__form-field--checkbox-comms"
                        onChange={checked => this.toggleCheckbox('comms', checked)}
                    />

                    <input type="hidden" ref="terms" value={this.state.terms}/>
                    <input type="hidden" ref="age" value={this.state.age}/>
                    <input type="hidden" ref="comms" value={this.state.comms}/>

                    <Touchable
                        x={1208}
                        y={570}
                        width={390}
                        height={49}
                        onPress={() => this.setState({isTermsShowing: true})}
                    />
                </div>

                {this.renderKeyboard()}

                {this.renderResult()}

                <Touchable
                    x={87}
                    y={947}
                    width={380}
                    height={98}
                    onPress={() => this.props.history.replace(`/${this.props.match.params.type}/record`)}
                />

                <div className={`Data__terms ${this.state.isTermsShowing ? 'Data__terms--active' : ''}`}>
                    <div className="Data__terms-inner">
                        <div className="Data__terms-title">Terms and Conditions</div>
                        <div className="Data__terms-content">
                            <Terms/>
                        </div>
                    </div>
                    <div className="Data__terms-close" onClick={() => this.setState({isTermsShowing: false})}>
                        &times;
                    </div>
                </div>
            </Screen>
        );
    }
}
