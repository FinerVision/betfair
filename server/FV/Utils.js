const getBindings = (fields = {}) => {
    const bindings = {};
    for (let field in fields) {
        if (!fields.hasOwnProperty(field)) continue;
        bindings[`$${field}`] = fields[field];
    }
    return bindings;
};

const guid = () => {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

module.exports = {getBindings, guid};
