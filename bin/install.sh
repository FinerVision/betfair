#!/usr/bin/env bash

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [[ ${machine} = "Linux" ]]
then
    # System dependencies
    sudo apt-get update -y
    sudo apt-get install ffmpeg libsqlite3-dev curl libcairo2-dev libjpeg-dev libpango1.0-dev libgif-dev build-essential g++ -y
    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    sudo apt-get install nodejs -y
    sudo ln -f -s /usr/bin/nodejs /usr/bin/node
fi

# Environment
cp env.example.js env.js

# Node dependencies
npm install

# Compile app
npm run build

# Autostart on Ubuntu
mkdir -p ~/.config/autostart
cp bin/bash.desktop ~/.config/autostart
