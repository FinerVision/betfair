#!/usr/bin/env bash

while getopts v: option
do
 case "${option}"
 in
 v) video=${OPTARG};;
 esac
done

if [[ -z "${video}" ]]
then
    echo "Video option -v is required"
    exit 0
fi

ffmpeg -i "${video}" -vf scale=1528:958 "${video}.new.mp4"
mv "${video}.new.mp4" "${video}"
