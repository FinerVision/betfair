const UploadController = require('./Controllers/UploadController');
const SubmitController = require('./Controllers/SubmitController');
const CollageController = require('./Controllers/CollageController');

module.exports = function (app, db) {
    app.post('/upload', UploadController.bind(null, db));
    app.post('/submit', SubmitController.bind(null, db));
    app.post('/collage/upload', CollageController.bind(null, db));
};
