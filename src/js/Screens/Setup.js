import React, {Component} from "react";
import config from "../../../config";
import Screen from "../Components/Screen";
import Touchable from "../Components/Touchable";
import Store from "fv-store";

export default class Setup extends Component {
    constructor(props) {
        super(props);

        this.locations = config.get('locations');
    }

    componentDidMount() {
        Store.unset('onlyUsePhotoApp');
    }

    selectLocation(location) {
        localStorage.setItem('location', location);
        this.props.history.replace('/start');
    }

    render() {
        return (
            <Screen name="Setup">
                {this.locations.filter(location => location.available).map((location, index) => (
                    <div key={index}>
                        <Touchable
                            x={location.x}
                            y={location.y}
                            width={location.width}
                            height={location.height}
                            onPress={() => this.selectLocation(location.id)}
                        />
                        <img
                            src={`img/locations/${location.id}.png`}
                            style={{position: 'absolute', left: `${location.x}px`, top: `${location.y}px`}}
                        />
                    </div>
                ))}
            </Screen>
        );
    }
}
