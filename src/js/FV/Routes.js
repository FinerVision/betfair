import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import Setup from "../Screens/Setup";
import Start from "../Screens/Start";
import Landing from "../Screens/Landing";
import Countdown from "../Screens/Countdown";
import Record from "../Screens/Record/index";
import Data from "../Screens/Data";
import Thanks from "../Screens/Thanks";
import HardwareError from "../Screens/Error";
import Info from "../Screens/Info";

export default () => (
    <Switch>
        <Route exact path="/" component={Setup}/>
        <Route exact path="/start" component={Start}/>
        <Route exact path="/error" component={HardwareError}/>
        <Route exact path="/info" component={Info}/>

        <Route exact path="/:type/landing" component={Landing}/>
        <Route exact path="/:type/countdown" component={Countdown}/>
        <Route exact path="/:type/record" component={Record}/>
        <Route exact path="/:type/data" component={Data}/>
        <Route exact path="/:type/thanks" component={Thanks}/>

        <Route>
            <Redirect to="/"/>
        </Route>
    </Switch>
);
