const fs = require('fs');
const superagent = require('superagent');
const config = require('../../config');

module.exports = function (db, data, callback) {
    let fileHash = '';

    if (data.type === 'photo') {
        fileHash = data.photo.split('uploads/').join('').split('.png').join('');
    } else {
        fileHash = data.video.split('uploads/').join('').split('.webm').join('').split('.mp4').join('');
    }

    const filePath = `${__dirname}/../../public/uploads/${fileHash}.${data.type === 'photo' ? 'png' : 'mp4'}`;

    if (!fs.existsSync(filePath)) {
        callback({success: false});
        return;
    }

    superagent
        .post(config.get('serverUrl'))
        .timeout(600000)
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .field('guid', data.guid)
        .field('first_name', data.first_name)
        .field('surname', data.surname)
        .field('email_address', data.email_address)
        .attach(data.type, filePath)
        .field('location', data.location)
        .field('age', data.age)
        .field('type', data.type)
        .field('comms', data.comms)
        .field('terms', data.terms)
        .field('device_id', fs.readFileSync(`${__dirname}/../device.txt`, 'utf8'))
        .field('created_at', data.created_at)
        .end((err, res) => {
            if (err) {
                console.error(err);
            }
            callback({success: !err && res.body.code === 200});
        });
};
