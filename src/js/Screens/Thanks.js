import React, {Component} from "react";
import Timer from "../FV/Timer";
import Screen from "../Components/Screen";

class Thanks extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        Timer.setTimeout(() => this.props.history.replace('/start'), 5000);
    }

    componentWillUnmount() {
        Timer.clearTimeouts();
    }

    render() {
        return <Screen name="Thanks"/>;
    }
}

export default Thanks;
