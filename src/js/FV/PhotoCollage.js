import request from "superagent";
import config from "../../../config";

export default class PhotoCollage {
    constructor() {
        this.canvas = document.createElement('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.canvas.width = config.get('collage.canvas.width');
        this.canvas.height = config.get('collage.canvas.height');
        this.imageSrcW = config.get('resolution.width');
        this.imageSrcH = config.get('resolution.height');
        this.imageW = config.get('collage.image.width');
        this.imageH = config.get('collage.image.height');
        this.background = new Image();
        this.background.src = 'img/collage/background.png';

        this.photoPositions = [
            {x: 16, y: 40},
            {x: 426, y: 40},
            {x: 16, y: 276},
            {x: 426, y: 276},
        ];

        this.clear();
    }

    clear() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    drawPhoto(photo, number) {
        this.ctx.save();
        this.ctx.scale(-1, 1);
        const x = -(this.imageW + this.photoPositions[number - 1].x);
        const y = this.photoPositions[number - 1].y;
        this.ctx.drawImage(photo, 0, 0, this.imageSrcW, this.imageSrcH, x, y, this.imageW, this.imageH);
        this.ctx.restore();
    }

    drawBackground() {
        this.ctx.drawImage(this.background, 0, 0, this.canvas.width, this.canvas.height);
    }

    save() {
        return new Promise((resolve, reject) => {
            this.canvas.toBlob(blob => {
                const formData = new FormData();
                formData.append('blob', blob);

                request
                    .post(`${config.get('url')}/collage/upload`)
                    .send(formData)
                    .end((err, res) => {
                        const photo = res.body.data.photo;
                        if (err || !photo) {
                            reject(err);
                            return;
                        }
                        resolve(photo);
                    });
            });
        });
    }
}
