import React, {Component} from "react";
import moment from "moment";
import Timer from "../../FV/Timer";
import Utils from "../../FV/Utils";
import Recorder from "../../FV/Recorder";
import config from "../../../../config/index";
import Screen from "../../Components/Screen";
import Touchable from "../../Components/Touchable";
import Event from "../../FV/Event";

export default class Video extends Component {
    constructor(props) {
        super(props);

        this.recorder = new Recorder();
        this.location = Utils.getLocation();

        this.state = {
            start: false,
            timer: config.get('env') === 'dev' ? 10 : this.location.timer,
            restart: false
        };
    }

    componentDidMount() {
        this.start();
        Timer.setTimeout(() => {
            this.setState({start: true}, () => this.recorder.start());
        }, 3000);
    }

    componentWillUnmount() {
        Timer.clearTimeouts();
        Timer.clearIntervals();
        this.recorder.cleanUp();
    }

    start() {
        this.recorder.request().then(() => {
            this.refs.camera.src = URL.createObjectURL(this.recorder.stream);

            this.recorder.on('start', () => {
                this.startTimer();
                this.refs.video.play();
            });

            this.recorder.on('stop', () => {
                this.recorder.upload().then(res => {
                    if (res.status !== 200) {
                        console.error('An error occurred');
                        return;
                    }
                    localStorage.setItem('video', res.data.video);
                    this.props.history.replace(`/${this.props.match.params.type}/data`);
                }).catch(err => console.error(err));
            });
        }).catch(err => {
            console.error('Record', err);
            // NOTE: Don't show error screen when in dev env.
            Event.emit('hardwareError');
        });
    }

    startTimer() {
        Timer.setInterval(() => {
            let {timer} = this.state;
            timer--;
            if (timer < 0) {
                this.recorder.stop();
                Timer.clearIntervals();
                return;
            }
            this.setState({timer});
        }, 1000);
    }

    getFormattedTime() {
        const {timer} = this.state;
        let seconds = moment.duration(timer, 'seconds').seconds();
        seconds = seconds < 10 ? `0${seconds}` : seconds;
        return `${moment.duration(timer, 'seconds').minutes()}:${seconds}`;
    }

    getVideoFileName() {
        return config.get('env') === 'dev' ? 'video/test.mp4' : `video/${this.location.id}.mp4`;
    }

    renderReload() {
        if (!this.state.restart) {
            return null;
        }

        return (
            <div className="Record__confirm-restart">
                <div className="Record__confirm-restart-background" onClick={() => this.setState({restart: false})}/>

                <div className="Record__confirm-restart-box">
                    <div className="Record__confirm-restart-box-title">Are you sure you want to restart?</div>

                    <div className="Record__confirm-restart-box-buttons">
                        <button onClick={() => location.reload()}>Yes</button>
                        <button onClick={() => this.setState({restart: false})}>No</button>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <Screen name="Record">
                <div className="Record__timer">
                    {this.getFormattedTime()}
                </div>

                <div className="Record__jerseys">
                    {this.location.jerseys.map((jersey, index) => (
                        <img key={index} className="Record__jerseys-jersey" src={`img/jerseys/${this.location.id}/${jersey}.png`}/>
                    ))}
                </div>

                <div className="Record__camera">
                    <video ref="camera" autoPlay={true} muted={true}/>
                </div>

                <div className="Record__video">
                    <video ref="video" src={this.getVideoFileName()} muted={true}/>
                </div>

                {this.state.start ? null : <div className="Record__intro-message">{this.location.jumps} to jump...</div>}

                <Touchable x={1528} y={970} width={380} height={98} onPress={() => this.setState({restart: true})} debounce={1500}/>

                {this.renderReload()}
            </Screen>
        );
    }
}
