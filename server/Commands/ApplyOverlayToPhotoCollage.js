const fs = require('fs');
const Promise = require('promise');
const Canvas = require('canvas');

// Create canvas.
// Draw image overlay based on age.
// Resolve promise.
module.exports = data => new Promise(async resolve => {
    const backgroundImage = new Canvas.Image;
    const overlayImage = new Canvas.Image;
    backgroundImage.src = fs.readFileSync(`${__dirname}/../../public/${data.photo}`);

    let overlayImagePath = `${__dirname}/../../public/img/collage/overlay`;
    overlayImagePath = `${overlayImagePath}/${data.location}/${data.age === 'under-25' ? 'unbranded' : 'branded'}.png`;
    overlayImage.src = fs.readFileSync(overlayImagePath);

    const canvas = new Canvas(backgroundImage.width, backgroundImage.height);
    const ctx = canvas.getContext('2d');

    ctx.drawImage(backgroundImage, 0, 0, backgroundImage.width, backgroundImage.height);
    ctx.drawImage(overlayImage, 0, 0, overlayImage.width, overlayImage.height);
    fs.writeFileSync(`${__dirname}/../../public/${data.photo}`, canvas.toBuffer());

    resolve();
});
