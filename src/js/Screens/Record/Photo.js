import React, {Component} from "react";
import Store from "fv-store";
import DeviceStream from "../../FV/DeviceStream";
import Event from "../../FV/Event";
import config from "../../../../config/index";
import PhotoCollage from "../../FV/PhotoCollage";
import Screen from "../../Components/Screen";
import Touchable from "../../Components/Touchable";

export default class Photo extends Component {
    constructor(props) {
        super(props);

        this.totalPhotos = 4;
        this.interval = null;
        this.timeout = null;
        this.type = this.props.match.params.type;
        this.countdownInterval = 1200;
        this.PhotoCollage = new PhotoCollage();
        this.countdown = [
            'Ready',
            'Hurry!',
            'Props Ready!'
        ];
        this.state = {
            countdown: 3,
            photoNumber: 1,
            photoTaken: false,
            showFlash: false,
            videoURL: null
        };
    }

    componentWillUnmount() {
        if (this.interval !== null) {
            clearInterval(this.interval);
        }
        if (this.timeout !== null) {
            clearTimeout(this.timeout);
        }
    }

    componentWillMount() {
        if (this.type === 'video' && Store.get('onlyUsePhotoApp')) {
            this.props.history.replace('/start');
        }
    }

    componentDidMount() {
        DeviceStream
            .getDevices()
            .then(ctx => {
                this.refs.video.srcObject = ctx.streams.video;
                this.refs.video.play();
                this.startCountdown();
            })
            .catch(() => Event.emit('hardwareError'));
    }

    /**
     * Set an interval that decrements the countdown to 0.
     */
    startCountdown() {
        this.setState({countdown: 3}, () => {
            this.interval = setInterval(() => {
                const countdown = Math.max(0, this.state.countdown - 1);
                this.setState({countdown});
                if (countdown === 0) {
                    this.takePhoto();
                    clearInterval(this.interval);
                    this.interval = null;
                }
            }, this.countdownInterval);
        });
    }

    takePhoto() {
        this.setState({showFlash: true});

        this.timeout = setTimeout(() => {
            this.refs.video.pause();

            if (this.state.photoNumber === 1) {
                this.PhotoCollage.drawBackground();
            }

            this.PhotoCollage.drawPhoto(this.refs.video, this.state.photoNumber);

            this.setState({photoTaken: true, showFlash: false}, () => {
                this.refs.video.play();

                if (this.state.photoNumber === this.totalPhotos) {
                    this.PhotoCollage
                        .save()
                        .then(photo => {
                            localStorage.setItem('photo', photo);
                            this.props.history.replace(`/${this.type}/data`);
                        })
                        .catch(() => {
                            // TODO: Find a better way to handle server errors.
                            location.href = config.get('url');
                        });
                }
            });
        }, 500);
    }

    nextPhoto() {
        if (!this.state.photoTaken || this.state.photoNumber === this.totalPhotos) {
            return;
        }

        const photoNumber = Math.min(this.state.photoNumber + 1, this.totalPhotos);
        this.setState({photoNumber, photoTaken: false}, () => this.startCountdown());
    }

    renderCountdown() {
        if (this.state.countdown === 0 && !this.state.photoTaken) {
            return null;
        }

        if (this.state.countdown === 0 && this.state.photoTaken) {
            return (
                <div className="Photo__countdown">
                    <div className="Photo__countdown-inner Photo__countdown--feedback">
                        <img src="img/feedback.png"/>
                    </div>
                </div>
            );
        }

        return (
            <div className="Photo__countdown">
                <div className="Photo__countdown-inner">
                    <div className="Photo__countdown-number">
                        {this.state.countdown}
                    </div>
                    <div className="Photo__countdown-instruction">
                        {this.countdown[this.state.countdown - 1]}
                    </div>
                    <img src="img/icons/camera.png"/>
                </div>
            </div>
        );
    }

    render() {
        return (
            <Screen name="Photo">
                <div className={`Photo__flash ${this.state.showFlash ? 'Photo__flash--show' : 'Photo__flash--hide'}`}/>

                <div className="Photo__count">
                    {this.state.photoNumber}/{this.totalPhotos}
                </div>

                <div className="Photo__video">
                    <video ref="video"/>
                </div>

                {this.renderCountdown()}

                <Touchable x={1289} y={970} width={619} height={98} onPress={() => this.nextPhoto()}/>
            </Screen>
        );
    }
}
