const moment = require('moment');
const Utils = require('../FV/Utils');
const ApplyOverlayToPhotoCollage = require('../Commands/ApplyOverlayToPhotoCollage');

module.exports = async (db, req, res) => {
    // TODO: Possibly validate the backend, even though the user can't access dev tools in Kiosk mode?
    // TODO: Possibly throw errors back to user, if something went catastrophically wrong?

    const fields = {
        guid: Utils.guid(),
        first_name: req.body.first_name,
        surname: req.body.surname,
        email_address: req.body.email_address,
        video: req.body.video || '',
        photo: req.body.photo || '',
        type: req.body.type,
        location: req.body.location,
        age: req.body.age,
        comms: req.body.comms === 'true' ? 1 : 0,
        terms: 1,
        sending_to_server: 0,
        sent_to_server: 0,
        created_at: moment().format('YYYY-MM-DD HH:mm:ss')
    };

    if (req.body.type === 'photo') {
        await ApplyOverlayToPhotoCollage(req.body);
    }

    db.run(
        "INSERT INTO `users` (`guid`, `first_name`, `surname`, `email_address`, `video`, `photo`, `type`, `location`, `age`, `comms`, `terms`, `sending_to_server`, `sent_to_server`, `created_at`) VALUES ($guid, $first_name, $surname, $email_address, $video, $photo, $type, $location, $age, $comms, $terms, $sending_to_server, $sent_to_server, $created_at)",
        Utils.getBindings(fields)
    );

    return res.json({status: 200, data: null});
};
