-- Up
CREATE TABLE `users` (
  `guid` TEXT,
  `first_name` TEXT,
  `surname` TEXT,
  `email_address` TEXT,
  `video` TEXT,
  `photo` TEXT,
  `type` TEXT,
  `location` TEXT,
  `age` TEXT,
  `comms` INTEGER DEFAULT 0,
  `terms` INTEGER DEFAULT 0,
  `sending_to_server` INTEGER DEFAULT 0,
  `sent_to_server` INTEGER DEFAULT 0,
  `created_at` TEXT
);

-- Down
DROP TABLE `users`;
