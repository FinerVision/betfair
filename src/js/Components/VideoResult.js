import React, {Component} from "react";
import PropTypes from "prop-types";
import moment from "moment";
import config from "../../../config";
import Utils from "../FV/Utils";
import Timer from "../FV/Timer";
import Event from "../FV/Event";
import DeviceStream from "../FV/DeviceStream";

class VideoResult extends Component {
    constructor(props) {
        super(props);

        this.location = Utils.getLocation();

        this.state = {
            timer: config.get('env') === 'dev' ? 10 : this.location.timer
        };
    }

    componentDidMount() {
        DeviceStream
            .getDevices()
            .then(ctx => {
                for (let i = 0; i < ctx.speakerIds.length; i++) {
                    const video = document.createElement('video');
                    video.autoplay = true;
                    video.src = this.props.video;
                    video.volume = config.get('volume');
                    video.setSinkId(ctx.speakerIds[i]);
                    this.refs.output.appendChild(video);
                }

                this.refs.camera.src = this.props.video;
                this.refs.camera.onplay = () => {
                    this.refs.video.play();
                    this.startTimer();
                };
            })
            .catch(err => {
                console.error('VideoResult', err);
                // NOTE: Don't show error screen when in dev env.
                Event.emit('hardwareError');
            });
    }

    componentWillUnmount() {
        Timer.clearIntervals();
    }

    startTimer() {
        Timer.setInterval(() => {
            let {timer} = this.state;
            timer--;
            if (timer < 0) {
                Timer.clearIntervals();
                return;
            }
            this.setState({timer});
        }, 1000);
    }

    getFormattedTime() {
        const {timer} = this.state;
        let seconds = moment.duration(timer, 'seconds').seconds();
        seconds = seconds < 10 ? `0${seconds}` : seconds;
        return `${moment.duration(timer, 'seconds').minutes()}:${seconds}`;
    }

    getVideoFileName() {
        return config.get('env') === 'dev' ? 'video/test.mp4' : `video/${this.location.id}.mp4`;
    }

    render() {
        return (
            <div className={`VideoResult ${this.props.className}`}>
                <div className="VideoResult__inner">
                    <video className="VideoResult__camera" ref="camera" autoPlay={true} muted={true}/>
                    <video className="VideoResult__video" ref="video" src={this.getVideoFileName()} muted={true}/>

                    <div ref="output" style={{
                        width: '1px',
                        height: '1px',
                        position: 'absolute',
                        top: '-1px',
                        left: '-1px',
                        overflow: 'hidden'
                    }}/>
                </div>
            </div>
        );
    }
}

VideoResult.propTypes = {
    video: PropTypes.string.isRequired,
    location: PropTypes.object.isRequired,
    className: PropTypes.string
};

VideoResult.defaultProps = {
    className: ''
};

export default VideoResult;
