import React, {Component} from "react";
import {withRouter} from "react-router-dom";
import Store from "fv-store";
import config from "../../../config";
import DeviceStream from "../FV/DeviceStream";
import Admin from "../Components/Admin";
import ErrorMessage from "./ErrorMessage";
import Routes from "../FV/Routes";
import Event from "../FV/Event";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
            loading: true
        };
    }

    componentDidMount() {
        if (config.get('env') === 'prod') {
            document.addEventListener('contextmenu', event => event.preventDefault());
            document.addEventListener('selectstart', event => event.preventDefault());
        }

        Event.addListener('hardwareError', () => {
            // Only show the error screen when on the video record
            // screen and config is set to handle errors.
            if (this.props.location.pathname === '/video/record' && config.get('handleErrors')) {
                this.props.history.replace('/error');
            }
        });

        DeviceStream
            .getDevices()
            .then(ctx => {
                for (let i = 0; i < ctx.streams.audio.length; i++) {
                    const audio = document.createElement('audio');
                    audio.autoplay = true;
                    audio.src = URL.createObjectURL(ctx.streams.audio[i]);
                    audio.volume = config.get('volume');
                    audio.setSinkId(ctx.speakerIds[ctx.speakerIds.length - (i + 1)]);
                    document.querySelector('#app').appendChild(audio);
                }

                this.setState({loading: false}, () => this.checkDevices());
            })
            .catch(() => {
                this.setState({loading: false}, () => {
                    Event.emit('hardwareError');
                    this.checkDevices();
                });
            });
    }

    checkDevices() {
        setTimeout(() => {
            DeviceStream
                .getDevices()
                .then(() => {
                    if (['/error'].indexOf(this.props.location.pathname) > -1) {
                        location.href = config.get('url');
                        return;
                    }
                    this.checkDevices();
                })
                .catch(err => {
                    console.error(err);
                    Event.emit('hardwareError');
                    this.checkDevices();
                });
        }, 250);
    }

    render() {
        if (this.state.loading) {
            return (
                <div className="Fullscreen Fullscreen--center">
                    <h1>Loading...</h1>
                </div>
            );
        }

        return (
            <div className={`Wrapper ${config.get('env') === 'dev' ? 'Wrapper--dev' : ''}`}>
                <Admin {...this.props}/>
                <ErrorMessage error={this.state.error}/>
                <Routes/>
            </div>
        );
    }
}

export default withRouter(App);
