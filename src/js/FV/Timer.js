export default {
    timeouts: [],
    intervals: [],

    setTimeout(func, delay) {
        const timeout = setTimeout(func, delay);
        this.timeouts.push(timeout);
        return this.timeouts.length - 1;
    },

    setInterval(func, delay) {
        const interval = setInterval(func, delay);
        this.intervals.push(interval);
        return this.intervals.length - 1;
    },

    clearTimeout(timeout) {
        const index = this.timeouts.indexOf(timeout);
        if (index > -1) {
            clearTimeout(this.timeouts[index]);
            this.timeouts.splice(index, 1);
        }
    },

    clearInterval(interval) {
        const index = this.intervals.indexOf(interval);
        if (index > -1) {
            clearInterval(this.intervals[index]);
            this.intervals.splice(index, 1);
        }
    },

    clearTimeouts() {
        this.timeouts.map(timeout => clearTimeout(timeout));
        delete this.timeouts;
        this.timeouts = [];
    },

    clearIntervals() {
        this.intervals.map(interval => clearInterval(interval));
        delete this.intervals;
        this.intervals = [];
    }
};
