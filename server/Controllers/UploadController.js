const fs = require('fs');
const crypto = require('crypto');

module.exports = function (db, req, res) {
    const blob = req.files.blob;

    if (!blob) {
        console.error('Blob was not sent in files.');
        return res.json({status: 400, message: 'Blob was not sent in files.'});
    }

    // Read temp file data.
    fs.readFile(blob.path, (err, data) => {
        if (err) {
            console.error(err);
            return res.json({status: 400, message: err.message});
        }

        // Create unique file hash.
        const fileHash = `${Math.floor((+new Date() / 1000))}-${crypto.randomBytes(16).toString('hex')}`;
        const fileDirectory = `${__dirname}/../../public/uploads`;

        // Save file to public uploads directory
        fs.writeFile(`${fileDirectory}/${fileHash}.webm`, data, err => {
            if (err) {
                console.error(err);
                return res.json({status: 400, data: {error: err.message}});
            }

            return res.json({status: 200, data: {video: `uploads/${fileHash}.webm`}});
        });
    });
};
