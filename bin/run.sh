#!/usr/bin/env bash

cd ~/Betfair

npm run start &
sleep 1 &&
google-chrome --start-fullscreen --disable-pinch --allow-insecure-localhost --overscroll-history-navigation=0 http://localhost:3000
