const mix = require('laravel-mix');

mix.options({
    processCssUrls: false
});

mix.react('src/js/app.js', 'public/js/app.js');
mix.sass('src/sass/app.scss', 'public/css/app.css');
