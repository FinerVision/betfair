import React, {Component} from "react";
import PropTypes from "prop-types";
import Utils from "../FV/Utils";
import Timer from "../FV/Timer";

class Touchable extends Component {
    constructor(props) {
        super(props);
        this.timeout = null;
        this.allowClick = true;
    }

    componentWillUnmount() {
        Timer.clearTimeout(this.timeout);
    }

    handleClick() {
        if (this.props.onPress) {
            if (!this.props.debounce) {
                this.props.onPress();
                return;
            }

            if (!this.allowClick) {
                return;
            }

            this.props.onPress();
            this.allowClick = false;
            Timer.clearTimeout(this.timeout);
            this.timeout = Timer.setTimeout(() => this.allowClick = true, this.props.debounce);
        }
    }

    handleMouseDown() {
        if (this.props.onPressIn) {
            this.props.onPressIn();
        }
    }

    handleMouseUp() {
        if (this.props.onPressUp) {
            this.props.onPressUp();
        }
    }

    getStyle() {
        return {
            width: `${Utils.scale(this.props.width, 'x')}%`,
            height: `${Utils.scale(this.props.height, 'y')}%`,
            left: `${Utils.scale(this.props.x, 'x')}%`,
            top: `${Utils.scale(this.props.y, 'y')}%`
        };
    }

    render() {
        return (
            <div
                className={`Touchable ${this.props.className}`}
                style={this.getStyle()}
                onClick={() => this.handleClick()}
                onMouseDown={() => this.handleMouseDown()}
                onMouseUp={() => this.handleMouseUp()}
            >
                {this.props.children}
            </div>
        );
    }
}

Touchable.propTypes = {
    onPress: PropTypes.func,
    onPressIn: PropTypes.func,
    onPressUp: PropTypes.func,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    className: PropTypes.string,
    debounce: PropTypes.number
};

Touchable.defaultProps = {
    className: ''
};

export default Touchable;
