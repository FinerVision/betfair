const commands = require('./commands');

module.exports = function (db) {
    // When the app is reset whilst users are being uploaded to the server,
    // sending_to_server will be set to 1 and never change. This fixes this
    // issue by resetting all currently uploading users.
    db.run("UPDATE `users` SET `sending_to_server` = 0 WHERE `sent_to_server` = 0");

    commands(db);
};
