import React, {Component} from "react";
import Photo from "./Photo";
import Video from "./Video";

export default class Record extends Component {
    constructor(props) {
        super(props);
        this.type = this.props.match.params.type;
    }

    render() {
        if (this.type === 'photo') {
            return <Photo {...this.props}/>;
        }
        return <Video {...this.props}/>;
    }
}
