import React, {Component} from "react";
import Store from "fv-store";
import Utils from "../FV/Utils";
import Screen from "../Components/Screen";
import Touchable from "../Components/Touchable";

export default class Landing extends Component {
    constructor(props) {
        super(props);

        this.location = Utils.getLocation();
        this.type = this.props.match.params.type;
    }

    componentWillMount() {
        if (this.type === 'video' && Store.get('onlyUsePhotoApp')) {
            this.props.history.replace('/start');
        }
    }

    nextScreen() {
        if (this.type === 'photo') {
            this.props.history.replace(`/${this.type}/record`);
            return;
        }
        this.props.history.replace(`/${this.type}/countdown`);
    }

    render() {
        return (
            <Screen
                name="Landing"
                style={{backgroundImage: `url(img/screens/${this.type}/landing-${this.location.id}.png)`}}
            >
                <Touchable
                    x={115}
                    y={799}
                    width={763}
                    height={194}
                    onPress={() => this.nextScreen()}
                />
            </Screen>
        );
    }
}
