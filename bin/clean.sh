#!/usr/bin/env bash

# Stop all node processes
killall node

rm -f db/database.sqlite
rm -f public/uploads/*
rm -rf node_modules
