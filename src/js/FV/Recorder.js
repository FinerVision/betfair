import request from "superagent";
import config from "../../../config";
import DeviceStream from "./DeviceStream";

class Recorder {
    constructor(props = {}) {
        this.settings = {
            capture: {
                audio: true,
                video: {
                    width: {exact: 1920},
                    height: {exact: 1080},
                    frameRate: 30
                }
            },
            recorder: {
                videoBitsPerSecond: 2000000,
                mimeType: 'video/webm;codecs=h264'
            }
        };

        this.stream = null;
        this.recorder = null;
        this.audioContext = null;
        this.chunks = [];
        this.events = {};

        Object.assign(this.settings, props);
    }

    on(event, callback) {
        this.events[event] = callback;
    }

    off(event) {
        if (this.events.hasOwnProperty(event)) {
            delete this.events[event];
        }
    }

    emit(event) {
        if (this.events.hasOwnProperty(event)) {
            this.events[event]();
        }
    }

    request() {
        return new Promise((resolve, reject) => {
            DeviceStream
                .getDevices()
                .then(ctx => {
                    this.mix(ctx.streams.audio).then(audioTrack => {
                        const videoTrack = ctx.streams.video.getTracks()[0];

                        this.stream = new MediaStream([videoTrack, audioTrack]);
                        this.recorder = new MediaRecorder(this.stream, this.settings.recorder);

                        this.recorder.onstart = () => {
                            this.chunks = [];
                            this.emit('start');
                        };

                        this.recorder.ondataavailable = event => this.chunks.push(event.data);
                        this.recorder.onstop = () => this.emit('stop');

                        resolve();
                    });
                })
                .catch(reject);
        });
    }

    mix(streams = []) {
        const getAudioTrack = () => {
            this.audioContext = new AudioContext();
            const dest = this.audioContext.createMediaStreamDestination();
            streams.map(stream => {
                const source = this.audioContext.createMediaStreamSource(stream);
                source.connect(dest);
            });
            return dest.stream.getTracks()[0];
        };

        return new Promise(resolve => {
            if (this.audioContext !== null && this.audioContext.state === 'closed') {
                this.audioContext.close().then(resolve(getAudioTrack()));
                return;
            }
            resolve(getAudioTrack());
        });
    }

    start() {
        if (this.recorder === null) {
            return;
        }
        this.recorder.start();
    }

    stop() {
        if (this.stream === null) {
            return;
        }
        this.recorder.stop();
    }

    cleanUp() {
        return new Promise(resolve => {
            if (this.stream === null) {
                return;
            }

            delete this.chunks;
            this.chunks = [];
            URL.revokeObjectURL(this.stream);

            if (this.audioContext !== null && this.audioContext.state === 'closed') {
                this.audioContext.close().then(() => resolve());
                return;
            }
            resolve();
        });
    }

    upload() {
        return new Promise((resolve, reject) => {
            const blob = new Blob(this.chunks);
            const formData = new FormData();
            formData.append('blob', blob);

            request
                .post(`${config.get('url')}/upload`)
                .accept('application/json')
                .send(formData)
                .end((err, res) => {
                    if (err) {
                        reject(err);
                        return;
                    }
                    resolve(res.body);
                });
        });
    }
}

export default Recorder;
