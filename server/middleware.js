const formData = require('express-form-data');

module.exports = function (express, app) {
    app.use(express.static(`${__dirname}/../public/`));
    app.use(formData.parse({autoFiles: true}));
    app.use(formData.format());
    app.use(formData.stream());
    app.use(formData.union());
};
