import React, {Component} from "react";
import PropTypes from "prop-types";

class Checkbox extends Component {
    constructor(props) {
        super(props);
    }

    handleChange() {
        if (this.props.onChange) {
            this.props.onChange(!this.props.checked);
        }
    }

    render() {
        return (
            <div className={`Checkbox ${this.props.className}`}>
                <img src={`img/checkbox/${this.props.color}/${this.props.checked ? 'checked' : 'unchecked'}.png`}/>
                <input type="checkbox" checked={this.props.checked} onChange={() => this.handleChange()}/>
            </div>
        );
    }
}

Checkbox.propTypes = {
    onChange: PropTypes.func,
    className: PropTypes.string,
    color: PropTypes.oneOf(['black', 'white']),
    defaultValue: PropTypes.bool,
    checked: PropTypes.bool.isRequired
};

Checkbox.defaultProps = {
    className: '',
    color: 'white',
    checked: false
};

export default Checkbox;
