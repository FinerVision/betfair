# BetFair

### Ubuntu Setup

Generate SSH key for machine

```
$ ssh-keygen
```

Install basic dependencies

```
$ sudo apt-get install git xsel -y
```

Add public SSH key to Bitbucket – https://bitbucket.org/account/ssh-keys/

```
$ xsel --clipboard <<< $(cat ~/.ssh/id_rsa.pub)
```

Close repository

```
$ git clone git@bitbucket.org:FinerVision/betfair.git ~/Betfair
```

### Installation

```
$ ./bin/install.sh
```

### Notes

**Make sure all hardware is correctly connected, including: two
headphones and one camera.**

Events are on the following dates

- Haydock – 25th November
- Sandown Park – 9th December
- Newbury – Exp 10th February
- Ascot – Exp 17th February
